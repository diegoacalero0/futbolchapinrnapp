import React, {Component} from 'react';

import Splash from './Splash'

import {StackActions, NavigationActions} from 'react-navigation'

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'home'})],
});

const resetActionToActive = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'activeNotifications'})],
});

export default class Welcome extends Component{
    constructor(props){
        super(props);
        this.state = {timePassed: false, notificationsActive: false}
    }

    componentDidMount(){
        setTimeout(() => {
          this.setTimePassed();
        }, 1500);
    }

    setTimePassed(){
        this.props.navigation.dispatch(resetAction);
    }
    

    render(){
        if(!this.state.timePassed){
            return <Splash/>
        }else{
            return null;
        }
    }

}