import React, {Component} from 'react';
import {Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter';
var parseString = require('react-native-xml2js').parseString;

const {width, height} = Dimensions.get('window');

import { 
    AdMobBanner, 
    AdMobInterstitial, 
    PublisherBanner,
    AdMobRewarded
  } from 'react-native-admob'

export default class Positions extends Component{

    constructor(props){
        super(props);
        this.state = {teams: {}, refreshing: true, teams: [], teamsCum: [], showCum: true,
        icons: {
            6546: require('../resources/logos/6546.png'),
            6539: require('../resources/logos/6539.png'),
            6538: require('../resources/logos/6538.png'),
            5751: require('../resources/logos/5751.png'),
            5155: require('../resources/logos/5155.png'),
            5154: require('../resources/logos/5154.png'),
            4417: require('../resources/logos/4417.png'),
            2248: require('../resources/logos/2248.png'),
            2247: require('../resources/logos/2247.png'),
            2067: require('../resources/logos/2067.png'),
            1773: require('../resources/logos/1773.png'),
            1292: require('../resources/logos/1292.png'),
            6549: require('../resources/logos/6549.png'),
            6540: require('../resources/logos/6540.png')
        },
        textHelp: 'Clausura'  
    }
    }



    getTeams(){
        fetch('http://futbolchapinenvivo.com/xml/es/guatemala/deportes.futbol.guatemala.posiciones.xml')
        .then(response => {
            let xmlPositions = response._bodyInit

            let promise = new Promise((resolve, reject) => {
                parseString(xmlPositions, function (err, result) {
                    if(err)
                        reject('Error al leer la tabla de posiciones: ' + err);
                    else{
                        let teamsCum = [];
                        let teams = []
                        for(let i = 0; i < result.posiciones.equipo.length; i++){
                            teams.push(result.posiciones.equipo[i]);
                            teamsCum.push(result.posiciones.equipo[i]);
                        }
                
                        let len = teamsCum.length;
    
                        for (let i = 0; i < len ; i++) {
                            for(let j = 0; j < len - i - 1; j++){
    
                                let t1 = teamsCum[j];
                                let t2 = teamsCum[j + 1];
                                
                                t1.puntosactual[0] = parseInt(t1.puntosactual[0])
                                t1.difgolactual[0] = parseInt(t1.difgolactual[0])
    
                                t2.puntosactual[0] = parseInt(t2.puntosactual[0])
                                t2.difgolactual[0] = parseInt(t2.difgolactual[0])
                                
    
                                let op1 = t1.puntosactual[0] < t2.puntosactual[0];
                                let op2 = t1.puntosactual[0] == t2.puntosactual[0] && t1.difgolactual[0] < t2.difgolactual[0];
    
    
                                if (op1 || op2) {
                                    let temp = teamsCum[j];
                                    teamsCum[j] = teamsCum[j + 1];
                                    teamsCum[j + 1] = temp;
                                }
                            }
                        }
                        
                       
                        resolve({teams: teams, teamsCum: teamsCum, refreshing: false, textHelp: result.posiciones.campeonatoNombreAlternativo[0]['_'].split(' ')[0]});
                    }
                });
            })

            promise.then((data) => {

              

                this.setState(data)
            })
            .catch(err => {
                alert('Ha ocurrido un error en la conexión.')
            })

        })
        .catch(error => {
            alert('Ha ocurrido un error en la conexión.');
        })
    }

    componentWillMount(){
        AdMobInterstitial.setAdUnitID(Platform.OS === 'ios' ? 'ca-app-pub-3710973902746391/7881605041' : 'ca-app-pub-3710973902746391/2280373125');
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
        this.setState(this.getTeams());    
    }

    refresh = () => {
        this.getTeams();
    }

    renderTeam(item, index){
        if(this.state.showCum){
            return(
                <View>
                    <View style = {styles.team}>
                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {index + 1}
                            </Text>
                        </View>

                        <View style = {styles.teamNameTable}>
                            <Image
                                style={{width: 30, height: 30, alignSelf: 'center', marginRight: 5}}
                                source={this.state.icons[item['$'].id]}
                            />

                            <Text style = {styles.infoText}>
                                {(item.nombre[0].substr(0, 5) == 'Xelaj' ? 'Xelajú' : (item.nombre[0].substr(0, 8) == 'Siquinal' ? 'Siquinalá' : (item.nombre[0].substr(0, 3) == 'Cob' ? 'Cobán Imperial' : item.nombre[0])))}
                            </Text>

                        </View>

                        <View style = {styles.infoTablePoints}>
                            <Text style = {styles.infoTextPoints}>
                                {item.puntosactual[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.jugadosactual[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.difgolactual[0]}
                            </Text>
                        </View>
                    </View>

                    <View style = {{width: width, height: 1, backgroundColor: '#0f1235'}}/>
                    

                </View>
            )
        }else{
            return(
                <View>
                    <View style = {styles.team}>
                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {index + 1}
                            </Text>
                        </View>

                        <View style = {styles.teamNameTable}>
                            <Image
                                style={{width: 30, height: 30, alignSelf: 'center', marginRight: 5}}
                                source={this.state.icons[item['$'].id]}
                            />

                            <Text style = {styles.infoText}>
                                {(item.nombre[0].substr(0, 5) == 'Xelaj' ? 'Xelajú' : (item.nombre[0].substr(0, 8) == 'Siquinal' ? 'Siquinalá' : (item.nombre[0].substr(0, 3) == 'Cob' ? 'Cobán Imperial' : item.nombre[0])))}
                            </Text>

                        </View>

                        <View style = {styles.infoTablePoints}>
                            <Text style = {styles.infoTextPoints}>
                                {item.puntos[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.jugados[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.ganados[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.empatados[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.perdidos[0]}
                            </Text>
                        </View>

                        <View style = {styles.infoTable}>
                            <Text style = {styles.infoText}>
                                {item.difgol[0]}
                            </Text>
                        </View>
                    </View>

                    <View style = {{width: width, height: 1, backgroundColor: '#0f1235'}}/>
                    

                </View>
            )
        }
    }

    render(){
        return(
            <View style = {styles.container}>
                <Header navigation = {this.props.navigation}/>
                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        TABLA DE POSICIONES
                    </Text>
                </View>

                <View style = {styles.tableSelector}>
                    <TouchableOpacity
                        style = {[styles.button, (this.state.showCum ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(!this.state.showCum)
                                this.setState({showCum: true})
                        }}>
                        <Text style = {[styles.textButton, (this.state.showCum ? styles.cumText : styles.noCumText)]}>
                            Acumulada
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style = {[styles.button, (!this.state.showCum ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.showCum)
                                this.setState({showCum: false})
                        }}>
                        <Text style = {[styles.textButton, (!this.state.showCum ? styles.cumText : styles.noCumText)]}>
                            {this.state.textHelp}
                        </Text>
                    </TouchableOpacity>
                </View>               
                
                {this.state.showCum ?
                    <View style = {styles.tableHeader}>
                        <Text style = {styles.info}>
                            POS
                        </Text>

                        <Text style = {styles.teamName}>
                            EQUIPO
                        </Text>

                        <Text style = {styles.info}>
                            PTS
                        </Text>

                        <Text style = {styles.info}>
                            JJ
                        </Text>

                        <Text style = {styles.info}>
                            DIF
                        </Text>

                    </View> : 

                    <View style = {styles.tableHeader}>
                        <Text style = {styles.info}>
                            POS
                        </Text>

                        <Text style = {styles.teamName}>
                            EQUIPO
                        </Text>

                        <Text style = {styles.info}>
                            PTS
                        </Text>

                        <Text style = {styles.info}>
                            JJ
                        </Text>

                        <Text style = {styles.info}>
                            JG
                        </Text>

                        <Text style = {styles.info}>
                            JE
                        </Text>

                        <Text style = {styles.info}>
                            JP
                        </Text>

                        <Text style = {styles.info}>
                            DIF
                        </Text>

                    </View>
                
                }

                <View style = {styles.flatlist}>
                    <FlatList
                        refreshing = {this.state.refreshing}
                        onRefresh = {this.refresh}
                        data = {(this.state.showCum ? this.state.teamsCum : this.state.teams)}
                        renderItem = {({item, index}) => this.renderTeam(item, index)}
                        keyExtractor = {(item, index) => index.toString()}
                        style = {styles.scroll}
                        contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                    </FlatList>
                </View>

                <AdFooter/>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    flatlist: {
        borderColor: '#0f1235',
        flex: 1
    },

    scroll: {
        flex: 1,
        width: width,
        backgroundColor: 'white'
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    tableSelector: {
        flexDirection: 'row',
    },

    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 10,
        marginRight: 10,
        marginVertical: 10,
        borderRadius: 10,
        paddingVertical: 5
    },

    textButton: {
        fontWeight: 'bold',
        fontSize: 15
    },

    cum: {
        borderColor: '#fff',
        backgroundColor: '#0f1235',
    },

    noCum: {
        borderColor: '#0f1235',
        backgroundColor: '#fff',
    },

    cumText: {
        color: '#fff'
    },

    noCumText: {
        color: '#0f1235',
        borderColor: '#0f1235',
    },

    tableHeader: {
        flexDirection: 'row',
        backgroundColor: '#0f1235',
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

    team: {
        width: width,
        flexDirection: 'row',
        paddingVertical: 5
    },

    teamName: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 5,
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },

    info: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },

    teamNameTable: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: 5,
        fontSize: 15,
    },

    infoTable: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    infoText: {
        color: 'black',
        fontSize: 15
    },

    infoTablePoints: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#0f1235'
    },

    infoTextPoints: {
        color: 'white',
        fontSize: 15
    }

  });
  