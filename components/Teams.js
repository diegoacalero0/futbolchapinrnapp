import React, {Component} from 'react';
import {Alert, AsyncStorage, Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter'
var parseString = require('react-native-xml2js').parseString;

const {width, height} = Dimensions.get('window');

export default class Teams extends Component{
    constructor(props){
        super(props);
        this.state = {teams: [], matchs: [], refreshing: true,
            icons: {
                6546: require('../resources/logos/6546.png'),
                6539: require('../resources/logos/6539.png'),
                6538: require('../resources/logos/6538.png'),
                5751: require('../resources/logos/5751.png'),
                5155: require('../resources/logos/5155.png'),
                5154: require('../resources/logos/5154.png'),
                4417: require('../resources/logos/4417.png'),
                2248: require('../resources/logos/2248.png'),
                2247: require('../resources/logos/2247.png'),
                2067: require('../resources/logos/2067.png'),
                1773: require('../resources/logos/1773.png'),
                1292: require('../resources/logos/1292.png'),
                6549: require('../resources/logos/6549.png'),
                6540: require('../resources/logos/6540.png')
                }}
    }
    
    getTeams(){
        fetch('http://futbolchapinenvivo.com/xml/es/guatemala/deportes.futbol.guatemala.posiciones.xml')
        .then(response => {
            let xmlPositions = response._bodyInit

            let promise = new Promise((resolve, reject) => {
                parseString(xmlPositions, function (err, result) {
                    if(err)
                        reject('Error al leer la tabla de posiciones: ' + err);
                    else{
                        let teams = []
                        for(let i = 0; i < result.posiciones.equipo.length; i++){
                            teams.push(result.posiciones.equipo[i]);
                            
                        }
                
                        let len = teams.length;
    
                        for (let i = 0; i < len ; i++) {
                            for(let j = 0; j < len - i - 1; j++){
    
                                let t1 = teams[j];
                                let t2 = teams[j + 1];
                        
                                op1 = t1.nombre[0] > t2.nombre[0];

                                if (op1) {
                                    let temp = teams[j];
                                    teams[j] = teams[j + 1];
                                    teams[j + 1] = temp;
                                }
                            }
                        }
                        
                        resolve({teams: teams, refreshing: false});
                    }
                });
            })

            promise.then((data) => {
                this.setState(data)
            })
            .catch(err => {
                alert('Ha ocurrido un error en la conexión.')
            })

        })
        .catch(error => {
            alert('Ha ocurrido un error en la conexión.');
        })
    }

    componentWillMount(){
        this.setState(this.getTeams());    
    }

    refresh = () => {
        this.getTeams();
    }

    renderTeam(item, index){
        return(
            <TouchableOpacity 
                style = {[styles.team, {marginBottom: 5, marginTop: index == 0 ? 10 : 5}]}
                onPress = {() => {
                    console.log(item);
                    this.props.navigation.navigate('players', {team: item})
                }}>

                <Image
                    style={{width: 40, height: 40, alignSelf: 'center', marginRight: 10}}
                    source={this.state.icons[item['$'].id]}
                />

                <Text style = {styles.teamNameText}>
                    {(item.nombre[0].substr(0, 5) == 'Xelaj' ? 'Xelajú' : (item.nombre[0].substr(0, 8) == 'Siquinal' ? 'Siquinalá' : (item.nombre[0].substr(0, 3) == 'Cob' ? 'Cobán Imperial' : item.nombre[0])))}
                </Text>
            </TouchableOpacity>
        )
    }

    render(){
        return(
            <View style = {styles.container}>

                <Header navigation = {this.props.navigation}/>

                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        EQUIPOS
                    </Text>
                </View>

                <FlatList
                    refreshing = {this.state.refreshing}
                    onRefresh = {this.refresh}
                    data = {this.state.teams}
                    renderItem = {({item, index}) => this.renderTeam(item, index)}
                    keyExtractor = {(item, index) => index.toString()}
                    style = {styles.scroll}
                    contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                </FlatList>     

                <AdFooter/>

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    scroll: {
        flex: 1,
        width: width,
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    team: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingVertical: 10,
        width: width * 0.95,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#0f1235',
        flexDirection: 'row',
        paddingHorizontal: 10,
    },

    teamNameText: {
        fontSize: 20,
        color: 'black'
    }

  });
  