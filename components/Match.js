import React, {Component} from 'react';
import {StatusBar, ScrollView, Linking, Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter'

import { 
    AdMobBanner, 
    AdMobInterstitial, 
    PublisherBanner,
    AdMobRewarded
  } from 'react-native-admob'


const {width, height} = Dimensions.get('window');

export default class Match extends Component{

    constructor(props){
        super(props);
        this.state = {teamAlign: 0, showCum: 0, match: {}, refreshing: true, date: '1/1/2019 00:00:00 UTC-3', hours: 0, minutes: 0, events: [], eventsReverse: [],
        icons: {
            6546: require('../resources/logos/6546.png'),
            6539: require('../resources/logos/6539.png'),
            6538: require('../resources/logos/6538.png'),
            5751: require('../resources/logos/5751.png'),
            5155: require('../resources/logos/5155.png'),
            5154: require('../resources/logos/5154.png'),
            4417: require('../resources/logos/4417.png'),
            2248: require('../resources/logos/2248.png'),
            2247: require('../resources/logos/2247.png'),
            2067: require('../resources/logos/2067.png'),
            1773: require('../resources/logos/1773.png'),
            1292: require('../resources/logos/1292.png'),
            6549: require('../resources/logos/6549.png'),
            6540: require('../resources/logos/6540.png')
            },
        venue: {
            5645: require('../resources/estadios/5645.jpg'),
            1835: require('../resources/estadios/1835.jpg'),
            18352: require('../resources/estadios/1835-2.jpg'),
            5643: require('../resources/estadios/5643.jpg'),
            1698: require('../resources/estadios/1698.jpg'),
            1836: require('../resources/estadios/1836.jpg'),
            4034: require('../resources/estadios/4034.jpg'),
            3232: require('../resources/estadios/3232.jpg'),
            1415: require('../resources/estadios/1415.jpg'),
            4026: require('../resources/estadios/4026.jpg'),
            2106: require('../resources/estadios/2106.jpg'),
            6421: require('../resources/estadios/6421.jpg'),
            1705: require('../resources/estadios/1705.jpg'),
            2170: require('../resources/estadios/2170.jpg'),
            4025: require('../resources/estadios/4025.jpg'),
            5644: require('../resources/estadios/5644.jpg'),
            7056: require('../resources/estadios/7056.jpg')
            },
        playersObject: null, home: 'No definido', away: 'No definido', players: [], dtHome: undefined, dtAway: undefined, playersHome: [], playersAway: [], playersHomeSub: [], playersAwaySub: [], homeId: 0, awayId: 0}
    }


    getEvents(){
        console.log(this.state.match.matchId)
        fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/events/' + this.state.match.matchId + '.json')
        .then(response => response.json())
        .then(response => {

            let incidences = [];
            let players = response.players;
            let home = response.match.homeTeamName;
            let away = response.match.awayTeamName;
            let homeId = response.match.homeTeamId;
            let awayId = response.match.awayTeamId;

            playersHome = [];
            playersAway = [];
            playersHomeSub = [];
            playersAwaySub = [];
            dtHome = undefined;
            dtAway = undefined;
            for(let prop in players){
                if(players[prop].teamId == homeId){
                    if(players[prop].posnId == 5)
                        dtHome = players[prop];
                    else if(players[prop].substitute)
                        playersHomeSub.push(players[prop]);
                    else
                        playersHome.push(players[prop]);
                }else{
                    if(players[prop].posnId == 5)
                        dtAway = players[prop];
                    else if(players[prop].substitute)
                        playersAwaySub.push(players[prop]);
                    else
                        playersAway.push(players[prop]);
                }
            }

            for (let i = 0; i < playersHome.length; i++) {
                for(let j = 0 ; j < playersHome.length - i - 1; j++){
                    if(playersHome[j].posnId > playersHome[j + 1].posnId){
                        let temp = playersHome[j];
                        playersHome[j] = playersHome[j + 1]
                        playersHome[j + 1] = temp
                    }
                }
            }

            for (let i = 0; i < playersAway.length; i++) {
                for(let j = 0 ; j < playersAway.length - i - 1; j++){
                    if(playersAway[j].posnId > playersAway[j + 1].posnId){
                        let temp = playersAway[j];
                        playersAway[j] = playersAway[j + 1]
                        playersAway[j + 1] = temp
                    }
                }
            }

            for(let prop in response.incidences){
                if(prop != 'correctPasses' && prop != 'incorrectPasses' 
                && prop != 'sombreros' && prop != 'stealings' 
                && prop != 'throwIn'
                && prop != 'nutmegs'
                && prop != 'fouls'
                && prop != 'offsides' && prop != 'goals'){
                    for(let propIn in response.incidences[prop]){
                        response.incidences[prop][propIn].typeName = prop;
                        incidences.push(response.incidences[prop][propIn]);
                    }
                }
            }
            
            let len = incidences.length;
            
            for (let i = 0; i < len ; i++) {
                for(let j = 0 ; j < len - i - 1; j++){

                    let t1 = incidences[j].t;
                    let t2 = incidences[j + 1].t;

                    let op1 = t1.half < t2.half;
                    let op2 = t1.half == t2.half && t1.m < t2.m;
                    let op3 = t1.half == t2.half && t1.m == t2.m && t1.s < t2.s

                    if (op1 || op2 || op3) {
                        var temp = incidences[j];
                        incidences[j] = incidences[j + 1];
                        incidences[j + 1] = temp;
                    }
                }
            }
            this.setState({playersObject: players, refreshing: false, eventsReverse: incidences.reverse(), events: incidences, home, away, players, homeId, awayId, playersHome, playersAway, playersHomeSub, playersAwaySub, dtHome, dtAway})
        })
        .catch(error => {
            alert('Ha ocurrido un error en la conexión.')
            this.setState({refreshing: false})
        });
        
    }

 

    componentWillMount(){

        AdMobInterstitial.setAdUnitID(Platform.OS === 'ios' ? 'ca-app-pub-3710973902746391/7881605041' : 'ca-app-pub-3710973902746391/2280373125');
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
    
        let match = this.props.navigation.getParam('match', {});
        this.setState({match: match}, () => {
            let item = this.state.match;
            let dateString  = item.date.toString();
            let year        = dateString.substring(0,4);
            let month       = dateString.substring(4,6);
            let day         = dateString.substring(6,8);
            let date        = new Date(year, month-1, day);
            
            localTime = new Date('1/1/2018 ' + item.scheduledStart + ' UTC-3');
            let hours = localTime.getHours();
            hours = (hours <= 10 ? '0' + hours : hours);
            let minutes = localTime.getMinutes();
            minutes = (minutes < 10 ? minutes + '0' : minutes);

            this.setState({
                hours, minutes, date
            });

            this.getEvents();

        });



    }

    formatDate(date) {
        var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
        ];
      
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
      
        return monthNames[monthIndex] + ' ' + day + ' del ' + year;
    }

    renderMatch(){
        let item = this.state.match;
        return(
            <View style = {[styles.match, {marginBottom: 10, marginTop: 10}]}>
                    <Text style = {styles.dateText}>{this.formatDate(new Date(this.state.date))}</Text>
                    <Text style = {styles.hourText}>{this.state.hours + ':' + this.state.minutes}</Text>
    
                    <View style = {{flexDirection: 'row', flex: 1}}>
                        <View style = {styles.team1}>
                            <View style = {styles.infoTeam}>
                                <Image
                                    style={{width: 30, height: 30, alignSelf: 'center'}}
                                    source={this.state.icons[item.teams.homeTeamId]}
                                />
                                <Text style = {styles.nameTeam}>
                                    {item.teams.homeTeamName}
                                </Text>
                            </View>
                        </View>
    
                        <View style = {styles.score}>
                            <Text style = {styles.scoreTeam}>
                                {item.scoreStatus[item.teams.homeTeamId].score == null ? '-' : item.scoreStatus[item.teams.homeTeamId].score }
                            </Text>
                            <Text style = {styles.separator}>
                            -
                            </Text>
                            <Text style = {styles.scoreTeam}>
                                {item.scoreStatus[item.teams.awayTeamId].score == null ? '-' : item.scoreStatus[item.teams.awayTeamId].score}
                            </Text>
                        </View>
                        
    
                        <View style = {styles.team2}>
                            <View style = {styles.infoTeam}>
                                <Image
                                    style={{width: 30, height: 30, alignSelf: 'center'}}
                                    source={this.state.icons[item.teams.awayTeamId]}
                                />
                                <Text style = {styles.nameTeam}>
                                    {item.teams.awayTeamName}
                                </Text>
                            </View>
                        </View>
    
                    </View>
    
                    <View style = {styles.matchfooter}>
                        <View style = {styles.stadium}>
                            <Text style = {styles.stadiumText}>
                                {item.venueInformation.venue.stadium.stadiumName}
                            </Text>
                        </View>
                        <View style = {styles.status}>
                            <Text style = {styles.statusText}>
                                {item.statusId == 0 ? 'Sin iniciar' : (item.statusId == 1 ? 'En juego' : 'Finalizado')}
                            </Text>
                        </View>
                    </View>
                </View>
        )
    }

    getImage(item, typename, type){
        if(type == 9 || type == 10 || type == 11 || type == 12 || type ==13)
            return require('../resources/ball.png');
        if(type == 4)
            return require('../resources/cards.png');
        if(type == 3)
            return require('../resources/yellow.png');
        if(type == 5)
            return require('../resources/red.png');
        return require('../resources/message.png');
    }

    getMessage(item, typename, type){
        switch(type){
            case 1:
                return 'Bienvenidos a la transmisión del partido '
                + this.state.match.teams.homeTeamName + ' vs ' + this.state.match.teams.awayTeamName + '.'
            case 2:
                return "Fin del partido. "
                + this.state.match.teams.homeTeamName
                + ' '
                + this.state.match.scoreStatus[this.state.match.teams.homeTeamId].score
                + ' - '
                + this.state.match.scoreStatus[this.state.match.teams.awayTeamId].score
                + ' '
                + this.state.match.teams.awayTeamName + '.'
            case 3:
                return 'Tarjeta amarilla para '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' de ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 4:
                return 'Tarjeta roja por doble amarilla para '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' de ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 5:
                return 'Tarjeta roja para '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' de ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 6:
                return 'Cambio en '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + ' entra: '
                + this.state.players[item.inId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.inId].name.first.substring(0, 1) 
                + '.sale '
                + this.state.players[item.offId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.offId].name.first.substring(0, 1)
                + '.'
            case 7:
                return 'Cambio en '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + ' entra '
                + this.state.players[item.inId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.inId].name.first.substring(0, 1) 
                + '. sale '
                + this.state.players[item.offId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.offId].name.first.substring(0, 1)
                + '.'
            case 8:
                return 'Se ha suspendido el partido';
            case 9:
                return 'Goooool! de jugada por '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' para ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 10:
                return 'Goooool en propia puerta de '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' para ' 
                + (this.state.homeId != item.team ? this.state.home : this.state.away)
                + '.'
            case 11:
                return 'Goooool! de cabeza por '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' para ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 12:
                return 'Goooool! de tiro libre por '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' para ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 13:
                return 'Goooool! de penal por '
                + this.state.players[item.plyrId].name.last.split(' ')[0]
                + ' '
                + this.state.players[item.plyrId].name.first.substring(0, 1)
                + '. '
                + ' para ' 
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 17:
                return 'Termina el primer tiempo.'
            case 18:
                return 'Comienza el segundo tiempo.'
            case 28:
                return 'Tiro de esquina para '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 29:
                return 'Despeja el '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 30:
                return 'Despeja a corner el '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 33:
                return 'Disparo y afuera del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'   
            case 34:
                return 'Disparo al palo del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'   
            case 35:
                return 'Disparo al arco del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 42:
                return 'Penal atajado del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 43:
                return 'Penal atajado del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 44:
                return 'Penal desviado del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 45:
                return 'Penal al palo del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.' 
            case 48:
                return 'Saque de meta por parte del '
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 49:
                return "Finaliza el segundo tiempo del partido. "
                + this.state.match.teams.homeTeamName
                + ' '
                + this.state.match.scoreStatus[this.state.match.teams.homeTeamId].score
                + ' - '
                + this.state.match.scoreStatus[this.state.match.teams.awayTeamId].score
                + ' '
                + this.state.match.teams.awayTeamName + '.'
            case 50:
                return 'Inicia el primer alargue del partido.'
            case 51:
                return 'Finaliza el primer alargue del partido.'
            case 52:
                return 'Inicia el segundo alargue del partido.'
            case 53:
                return 'Finaliza el segundo alargue del partido.'
            case 54:
                return 'Inicia la ronda de penales.'
            case 55:
                return 'Goooool de penal! por'
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.' 
            case 56:
                return 'Penal atajado de'
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            case 57:
                return 'Penal desviado de'
                + (this.state.homeId == item.team ? this.state.home : this.state.away)
                + '.'
            default:
                return "Tipo: " + type + " nombre: " + typename
        }
    }

    renderEvent(item, index){
        return(
            <View>
                <View style = {styles.event}>
                    <View style = {styles.time}>
                        <Text style = {styles.textTime}>
                            {item.t.m + "'"}
                        </Text>
                    </View>

                    <View style = {styles.icon}>
                        <Image
                            style = {{width: 20, height: 20}}
                            source={this.getImage(item, item.typeName, item.type)}
                        />
                    </View>

                    <View style = {styles.message}>
                        <Text style = {styles.textMessage}>
                            {this.getMessage(item, item.typeName, item.type)}
                        </Text>
                    </View>

                </View>

                <View style = {{width: width, height: 1, backgroundColor: '#bebebe'}}/>
            </View>
        )
    }



    refresh = () => {
        this.getEvents();
    }

    renderVenue(){
    let source = this.state.venue[this.state.match.venueInformation.venue.venueId];
    if(this.state.match.teams.homeTeamId == 6540 && this.state.match.venueInformation.venue.venueId == 1835){
        source = this.state.venue[18352]
    }
       return(
            <View style = {{width: width}}>
                <View style = {[styles.titleAlign, {marginBottom: 5}]}>
                    <Text style = {styles.titleAlignText}>
                        Estadio
                    </Text>
                </View>  
                <View style = {styles.venueImg}>
                    <Image
                        resizeMode="cover"
                        style={{height: 200, width: null}}
                        source={source}
                    />
                </View>
            </View>
       )
    }

    renderEvent2(item, index){

        if(item.type != 3 && item.type != 4 && item.type != 5 && item.type != 9 && item.type != 10 && item.type != 11 && item.type != 12 && item.type != 13){
            return(
                index == this.state.events.length - 1 ?
                    this.renderVenue()
                : null
            )
        }
        return(
            <View>
                <View style = {styles.event}>

                    {item.team == this.state.homeId ? 
                        <View style = {styles.event}>

                            <View style = {styles.icon}>
                                <Image
                                    style = {{width: 20, height: 20}}
                                    source={this.getImage(item, item.typeName, item.type)}
                                />
                            </View>

                            <View style = {styles.time}>
                                <Text style = {styles.textTime}>
                                    {item.t.m + "'"}
                                </Text>
                            </View>

                            

                            <View style = {styles.message}>
                                <Text style = {styles.textMessage}>
                                    {this.state.playersObject !== null ? this.state.playersObject[item.plyrId].name.last.split(' ')[0]
                                    + ' '
                                    + this.state.playersObject[item.plyrId].name.first.substr(0, 1) + '.' : 'Desconocido'}
                                </Text>
                            </View>
                        </View>
                        :
                        <View style = {styles.event}>
                            <View style = {styles.message}>
                                <Text style = {styles.textMessageRight}>
                                    {this.state.playersObject !== null ? this.state.playersObject[item.plyrId].name.last.split(' ')[0]
                                    + ' '
                                    + this.state.playersObject[item.plyrId].name.first.substr(0, 1) + '.' : 'Desconocido'}
                                </Text>
                            </View>

                            <View style = {styles.time}>
                                <Text style = {styles.textTime}>
                                    {item.t.m + "'"}
                                </Text>
                            </View>

                            <View style = {styles.icon}>
                                <Image
                                    style = {{width: 20, height: 20}}
                                    source={this.getImage(item, item.typeName, item.type)}
                                />
                            </View>
                        </View>
                    }

                    

                </View>
                <View style = {{width: width, height: 1, backgroundColor: '#bebebe'}}/>
                {index == this.state.events.length - 1 ?
                    this.renderVenue()
                : null}

            </View>
        )
    }

    renderEventos(){
        return(
            <View style = {{flex: 1}}>
                <View style = {styles.flatlist}>
                    

                    {this.state.eventsReverse.length == 0 ?
                        <View>
                            <Text style = {styles.noInfo}>
                                No hay información
                            </Text>
                            {this.renderVenue()}
                        </View>
                       
                    : 
                    <FlatList
                        refreshing = {this.state.refreshing}
                        onRefresh = {this.refresh}
                        data = {this.state.eventsReverse}
                        renderItem = {({item, index}) => this.renderEvent2(item, index)}
                        keyExtractor = {(item, index) => index.toString()}
                        style = {styles.scroll}
                        contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                    </FlatList>}

                </View>
            </View>
        )
        
    }

    renderPlayer(item){
        return(
            <View>
                <View style = {styles.player}>
                    <View style = {styles.squadNo}>
                        <Text style = {styles.squadNoText}>
                            {item.squadNo}
                        </Text>
                    </View>
                    <View style = {styles.name}>
                        <Text style = {styles.nameText}>
                            {item.name.first + (item.name.middle != null ? ' ' + item.name.middle + ' ' : ' ') + item.name.last}
                        </Text>
                    </View>
                    <View style = {styles.posN}>
                        <Text style = {styles.posNText}>
                            {item.posnId == 1 ? 'Arquero'
                            : (item.posnId == 2 ? 'Defensor'
                            : (item.posnId == 3 ? 'Volante'
                            : (item.posnId == 4 ? 'Delantero' : 'DT')))}
                        </Text>
                    </View>
                </View>

                <View style = {{width: width, height: 1, backgroundColor: '#0f1235'}}/>

            </View>
        )
    }

    renderAlineaciones(){
        return(
            <View style = {[{width: width}, styles.flatlist]}>
                <View style = {styles.tableSelector}>
                    <TouchableOpacity
                        style = {[styles.button, (this.state.teamAlign == 0 ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.teamAlign == 1)
                                this.setState({teamAlign: 0})
                        }}>
                        <Text style = {[styles.textButton, (this.state.teamAlign == 0 ? styles.cumText : styles.noCumText)]}>
                            Local
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style = {[styles.button, (this.state.teamAlign == 1 ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.teamAlign == 0)
                                this.setState({teamAlign: 1})
                        }}>
                        <Text style = {[styles.textButton, (this.state.teamAlign == 1? styles.cumText : styles.noCumText)]}>
                            Visitante
                        </Text>
                    </TouchableOpacity>
                </View>

                {this.state.playersHome.length + this.state.playersAway.length + this.state.playersAway.length + this.state.playersAwaySub == 0 ?
                    <View>
                        <Text style = {styles.noInfo}>
                            No hay información
                        </Text>
                </View> : null}

                <View style = {{flex: 1}}>
                    <ScrollView style = {styles.scroll}>
                        {this.state.teamAlign == 0 ?
                        <View>
                            <View style = {styles.titleAlign}>
                                <Text style = {styles.titleAlignText}>
                                    Titulares
                                </Text>
                            </View>

                            {this.state.playersHome.map((item, index) => {
                                return this.renderPlayer(item)
                            })}

                            <View style = {styles.titleAlign}>
                                <Text style = {styles.titleAlignText}>
                                    Suplentes
                                </Text>
                            </View>

                            {this.state.playersHomeSub.map((item, index) => {
                               return this.renderPlayer(item)
                            })}
                        </View>
                            :
                            <View>
                            <View style = {styles.titleAlign}>
                                <Text style = {styles.titleAlignText}>
                                    Titulares
                                </Text>
                            </View>

                            {this.state.playersAway.map((item, index) => {
                                return this.renderPlayer(item)
                            })}

                            <View style = {styles.titleAlign}>
                                <Text style = {styles.titleAlignText}>
                                    Suplentes
                                </Text>
                            </View>

                            {this.state.playersAwaySub.map((item, index) => {
                               return this.renderPlayer(item)
                            })}
                        </View>
                        }

                        {this.state.dtHome && this.state.dtAway?
                        <View style = {styles.titleAlign}>
                            <Text style = {styles.titleAlignText}>
                                
                                {   'DT ' + 
                                    (this.state.teamAlign == 0 && this.state.dtAway !== null && this.state.dtHome !== null?
                                    this.state.dtHome.name.first + (this.state.dtHome.name.middle != null ? ' ' + this.state.dtHome.name.middle + ' ' : ' ') + this.state.dtHome.name.last
                                    :
                                    this.state.dtAway.name.first + (this.state.dtAway.name.middle != null ? ' ' + this.state.dtAway.name.middle + ' ' : ' ') + this.state.dtAway.name.last)
                                }
                            </Text>
                        </View> : null}

                    </ScrollView>
                </View>
            </View>
           
        )
    }

    renderComentarios(){
        return(

            <View style = {{flex: 1}}>
                <View style = {styles.flatlist}>

                    {this.state.events.length == 0 ?
                    <View>
                        <Text style = {styles.noInfo}>
                            No hay información
                        </Text>
                    </View> : null}

                    <FlatList
                        refreshing = {this.state.refreshing}
                        onRefresh = {this.refresh}
                        data = {this.state.events}
                        renderItem = {({item, index}) => this.renderEvent(item, index)}
                        keyExtractor = {(item, index) => index.toString()}
                        style = {styles.scroll}
                        contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                    </FlatList>
                </View>

                <TouchableOpacity style = {styles.live}
                    onPress = {() => {
                        Linking.openURL('http://futbolchapinenvivo.com')
                    }}>
                    <Text style = {styles.liveText}>
                        Más información
                    </Text>
                </TouchableOpacity>
            </View>
            
        )
    }

    render(){
        return(
            <View style = {styles.container}>
                <Header navigation = {this.props.navigation}/>
                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        EVENTOS DEL PARTIDO
                    </Text>
                </View>

                {this.renderMatch()}

                <View style = {styles.tableSelector}>
                    <TouchableOpacity
                        style = {[styles.button, (this.state.showCum == 0 ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.showCum != 0)
                                this.setState({showCum: 0})
                        }}>
                        <Text style = {[styles.textButton, (this.state.showCum == 0 ? styles.cumText : styles.noCumText)]}>
                            Eventos
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style = {[styles.button, (this.state.showCum == 1 ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.showCum != 1)
                                this.setState({showCum: 1})
                        }}>
                        <Text style = {[styles.textButton, (this.state.showCum == 1 ? styles.cumText : styles.noCumText)]}>
                            Alineaciones
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style = {[styles.button, (this.state.showCum == 2 ? styles.cum : styles.noCum)]}
                        onPress = {() => {
                            if(this.state.showCum != 2)
                                this.setState({showCum: 2})
                        }}>
                        <Text style = {[styles.textButton, (this.state.showCum == 2? styles.cumText : styles.noCumText)]}>
                            En vivo
                        </Text>
                    </TouchableOpacity>
                </View>

                {this.state.showCum == 0 ? this.renderEventos() : (this.state.showCum == 1 ? this.renderAlineaciones() : this.renderComentarios())}
                <AdFooter/>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    flatlist: {
        borderTopWidth: 3,
        borderColor: '#0f1235',
        flex: 1
    },

    scroll: {
        flex: 1,
        width: width,
        backgroundColor: 'white'
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    titleAlign: {
        
        height: 25,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleAlignText: {
        fontSize: 12,
        color: '#fff',
        fontWeight: 'bold'
    },

    match: {
        height: 100,
        paddingTop: 5,
        width: width * 0.95,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#0f1235'
    },

    dateText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 14
    },

    hourText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 12
    },

    team1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    team2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    infoTeam: {
        alignContent: 'center',
        justifyContent: 'center',
    },

    nameTeam: {
        textAlign: 'center',
        alignSelf: 'center',
        color: '#000',
        fontSize: 16,
    },
    
    separator: {
        marginLeft: 5,
        marginRight: 5,
        color: '#000',
        fontSize: 20
    },

    scoreTeam: {
        color: '#000',
        fontSize: 20
    },

    score: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    matchfooter: {
        flexDirection: 'row',
        height: 15,
        backgroundColor: '#0f1235',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginTop: 5,
        paddingRight: 10,
        paddingLeft: 10,
    },

    stadium: {
        flex: 1,
        alignItems: 'flex-start'
    },

    status: {
        flex: 1,
        alignItems: 'flex-end'
    },

    statusText: {
        color: 'white',
        fontSize: 12
    },

    stadiumText: {
        color: 'white',
        fontSize: 12
    },

    event: {
        width: width,
        flexDirection: 'row',
        marginTop: 5,
        marginBottom: 5,
        paddingRight: 5
    },

    time: {
        flex: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },

    textTime: {
        color: 'black',
    },

    icon: {
        flex: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },

    message: {
        flex: 70,
    },

    textMessage: {
        color: 'black',
    },

    textMessageRight: {
        color: 'black',
        textAlign: 'right'
    },

    live: {
        width: width,
        height: 30,
        backgroundColor: '#0f1235',
        alignItems: 'center',
        justifyContent: 'center'
    },

    liveText: {
        fontSize: 12,
        color: 'white',
        textAlign: 'center'
    },

    tableSelector: {
        flexDirection: 'row',
    },

    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        marginLeft: 5,
        marginRight: 5,
        marginVertical: 10,
        borderRadius: 10,
        paddingVertical: 5
    },

    textButton: {
        fontWeight: 'bold',
        fontSize: 10
    },

    cum: {
        borderColor: '#fff',
        backgroundColor: '#0f1235',
    },

    noCum: {
        borderColor: '#0f1235',
        backgroundColor: '#fff',
    },

    cumText: {
        color: '#fff'
    },

    noCumText: {
        color: '#0f1235',
        borderColor: '#0f1235',
    },

    player: {
        width: width,
        flexDirection: 'row',
        alignItems: 'center',
        padding: 2
    },

    squadNo: {
        alignItems: 'center',
        justifyContent: 'center',
        width: width * 0.1,
    },

    name: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        width: width * 0.65,
    },

    posN: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        flex: 1
    },

    canvas: {
        position: 'relative',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },

    squadNoText: {
        color: '#000'
    },

    nameText: {
        color: '#000'
    },

    posNText: {
        color: '#000'
    },

    noInfo: {
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: 12,
        color: '#000',
        padding: 10
    },

  });
  