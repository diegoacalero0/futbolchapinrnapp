import React, {Component} from 'react';
import {Alert, AsyncStorage, Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter'


global.Buffer = global.Buffer || require('buffer').Buffer

var parseString = require('react-native-xml2js').parseString;
const iconv = require('iconv-lite');

const {width, height} = Dimensions.get('window');

export default class Scorers extends Component{
    constructor(props){
        super(props);
        this.state = {teams: [], scorers: [], matchs: [], refreshing: true,
            icons: {
                6546: require('../resources/logos/6546.png'),
                6539: require('../resources/logos/6539.png'),
                6538: require('../resources/logos/6538.png'),
                5751: require('../resources/logos/5751.png'),
                5155: require('../resources/logos/5155.png'),
                5154: require('../resources/logos/5154.png'),
                4417: require('../resources/logos/4417.png'),
                2248: require('../resources/logos/2248.png'),
                2247: require('../resources/logos/2247.png'),
                2067: require('../resources/logos/2067.png'),
                1773: require('../resources/logos/1773.png'),
                1292: require('../resources/logos/1292.png'),
                6549: require('../resources/logos/6549.png'),
                6540: require('../resources/logos/6540.png')
                },
            players: {
                6546: [],
                6539: [],
                6538: [],
                5751: [],
                5155: [],
                5154: [],
                4417: [],
                2248: [],
                2247: [],
                2067: [],
                1773: [],
                1292: [],
                6549: [],
                6540: []
                }}
    }

    getTeams(){
        return new Promise((resolve, reject) => {
            fetch('http://futbolchapinenvivo.com/xml/es/guatemala/deportes.futbol.guatemala.posiciones.xml')
            .then(response => {
                let xmlPositions = response._bodyInit


                parseString(xmlPositions, function (err, result) {
                    if(err)
                        reject('Error al leer la tabla de posiciones: ' + err);
                    else{
                        let teams = []
                        for(let i = 0; i < result.posiciones.equipo.length; i++){
                            teams.push(result.posiciones.equipo[i]);   
                        }                
                        resolve({teams: teams, refreshing: false});
                    }
                });

            })
            .catch(error => {
                reject('Error en la conexión: ' + error);
            })
        })
    }

    getTeam(id){
        return new Promise((resolve, reject) => {
            fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/statsCenter/teams/' + id + '.json')
            .then(response => response.json())
            .then(response => {
                let playersJson = response.players;
                resolve(response);
            })
            .catch(error => {
                reject(error);
            })
        });
    }
    
    getScorers(){
        
        fetch('http://futbolchapinenvivo.com/xml/es/guatemala/deportes.futbol.guatemala.goleadores.xml',
            {
                headers: {
                    'Content-type': 'text/plain; charset=iso-8859-1',
                    'charset': 'utf-8',
                }
            }
        )
        .then(response => response.text())
        .then(response => {
            let xmlPositions = response

            let promise = new Promise((resolve, reject) => {
                parseString(xmlPositions, function (err, result) {
                    if(err)
                        reject('Error al leer la tabla de posiciones: ' + err);
                    else{
                        let scorers = []

                        if(result.goleadores.persona){
                            for(let i = 0; i < result.goleadores.persona.length; i++){
                                scorers.push(result.goleadores.persona[i]);
                            }
                        }
                        resolve({scorers: scorers, refreshing: false});
                    }
                });
            })

            promise
            .then((data) => {
                this.setState(data)
            })
            .catch(err => {
                alert(err);
            })
        })
        .catch(error => {
            alert('Ha ocurrido un error en la conexión.' + error);
        })
    }

    componentWillMount(){
        this.setState(this.getScorers());
        
        this.getTeams()
        .then(data => {

            let teams = data.teams;
            let promises = [];

            for(let i = 0; i < teams.length; i++){
                promises.push(this.getTeam(teams[i]['$'].id))
            }

            Promise.all(promises)
            .then(values => {
                for(let i = 0; i < teams.length; i++){
                    teams[i].players = values[i].players;
                    teams[i].name = values[i].info.name
                }
            })
            .catch(err => {
                alert('Ha ocurrido un error en la conexión.');
            })

            this.setState({teams: teams, refreshing: false})
        })
        .catch(err => {
            alert('Ha ocurrido un error en la conexión.')
            this.setState({refreshing: false})
        })

    }

    refresh = () => {
        this.getScorers();
    }

    renderPlayer(item, index){
        
        item.nombre = "";

        for(let i = 0; i < this.state.teams.length; i++){
            if(item.equipo[0].$.id == this.state.teams[i].$.id && this.state.teams[i].players){
                item.nombre = this.state.teams[i].players[parseInt(item.$.id)].info.name.last.split(' ')[0]
                + ' ' + this.state.teams[i].players[parseInt(item.$.id)].info.name.first.substr(0, 1) + '.'
               
                item.teamName = this.state.teams[i].name
                item.time = this.state.teams[i].players[parseInt(item.$.id)].summary.minutesPlayed.qty
                break;
            }
        }

        return(
            <View>
                <View style = {styles.player}>
                    <View style = {styles.playerNameTable}>
                        <Text style = {styles.infoText}>
                            {item.nombre}
                        </Text>
                    </View>


                    <View style = {styles.infoTableTeam}>

                    <Image
                        style={{width: 25, height: 25, alignSelf: 'center', marginRight: 10}}
                        source={this.state.icons[item.equipo[0].$.id]}
                    
                    />

                        <Text style = {styles.infoText}>
                        {item.teamName}
                        </Text>
                    </View>

                    <View style = {styles.infoTable}>
                        <Text style = {styles.infoText}>
                            {item.goles}
                        </Text>
                    </View>

                    <View style = {styles.infoTable}>
                        <Text style = {styles.infoText}>
                            {item.time}
                        </Text>
                    </View>
                </View>

                <View style = {{width: width, height: 1, backgroundColor: '#0f1235'}}/>
                

            </View>
        )
    }

    render(){
        return(
            <View style = {styles.container}>

                <Header navigation = {this.props.navigation}/>

                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        GOLEADORES
                    </Text>
                </View>

                <View style = {styles.tableHeader}>
                    <Text style = {styles.playerName}>
                        NOMBRE
                    </Text>

                    <Text style = {styles.infoTeam}>
                        EQUIPO
                    </Text>

                    <Text style = {styles.info}>
                        G
                    </Text>

                    <Text style = {styles.info}>
                        MIN
                    </Text>
                </View> 

                <FlatList
                    refreshing = {this.state.refreshing}
                    onRefresh = {this.refresh}
                    data = {this.state.scorers}
                    renderItem = {({item, index}) => this.renderPlayer(item, index)}
                    keyExtractor = {(item, index) => index.toString()}
                    style = {styles.scroll}
                    contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                </FlatList>     

                <AdFooter/>

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    scroll: {
        flex: 1,
        width: width,
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    team: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingVertical: 10,
        width: width * 0.95,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#0f1235',
        flexDirection: 'row',
        paddingHorizontal: 10,
    },

    teamNameText: {
        fontSize: 20,
        color: 'black'
    },

    player: {
        width: width,
        flexDirection: 'row',
        paddingVertical: 5
    },

    teamNameText: {
        fontSize: 20,
        color: 'black',
        textAlign: 'center'
    },

    tableHeader: {
        flexDirection: 'row',
        backgroundColor: '#0f1235',
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5
    },

    playerName: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 5,
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },

    playerNameTable: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 5,
        fontSize: 12,
    },

    infoTable: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5
    },

    info: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold',
        marginRight: 5
    },

    infoTableTeam: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row'
    },

    infoTeam: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 5,
        color: '#fff',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },

    infoText: {
        color: 'black',
        fontSize: 12,
        textAlign: 'center'
    },

  });
  