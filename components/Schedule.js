import React, {Component} from 'react';
import {Alert, AsyncStorage, Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter';

var parseString = require('react-native-xml2js').parseString;

const {width, height} = Dimensions.get('window');

export default class Schedule extends Component{
    constructor(props){
        super(props);
        this.state = {timePassed: false, matchs: [], refreshing: true,
            icons: {
                6546: require('../resources/logos/6546.png'),
                6539: require('../resources/logos/6539.png'),
                6538: require('../resources/logos/6538.png'),
                5751: require('../resources/logos/5751.png'),
                5155: require('../resources/logos/5155.png'),
                5154: require('../resources/logos/5154.png'),
                4417: require('../resources/logos/4417.png'),
                2248: require('../resources/logos/2248.png'),
                2247: require('../resources/logos/2247.png'),
                2067: require('../resources/logos/2067.png'),
                1773: require('../resources/logos/1773.png'),
                1292: require('../resources/logos/1292.png'),
                6549: require('../resources/logos/6549.png'),
                6540: require('../resources/logos/6540.png')
                }}
    }

    refresh = () => {
        this.setState({refreshing: true}, () => {
            this.setState({refreshing: false});
        });
    }

    getMatchs(){
        return new Promise((resolve, reject) => {
            fetch('http://futbolchapinenvivo.com/xml/es/guatemala/deportes.futbol.guatemala.calendario.xml')
            .then(response => {
                let xml = response._bodyInit
                parseString(xml, function (err, result) {
                    if(err)
                        reject('Error al leer el calendario.');
                    else{
                        fechas = result.fixture.fecha;
                        let arr = [];
                        let promisesArr = [];

                        for(let i = 0; i < fechas.length; i++){
                            let partidos = fechas[i].partido;

                            for(let j = 0; j < partidos.length; j++){

                                partidos[j].scheduledStart = partidos[j].$.hora;
                                partidos[j].date = partidos[j].$.fecha;
                                partidos[j].levelName = fechas[i].$.nombrenivel;
                                partidos[j].level = fechas[i].$.nivel;
                                partidos[j].matchId = partidos[j].$.id;

                                let dateString  =   partidos[j].$.fecha.toString();
                                let year        = dateString.substring(0,4);
                                let month       = dateString.substring(4,6);
                                let day         = dateString.substring(6,8);
                                let date        = new Date(year, month-1, day, 0, 0, 0);

                                arr.push(partidos[j]);
                                promisesArr.push(
                                    new Promise( (resolve, reject) => {
                                        fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/events/' + partidos[j].$.id + '.json')
                                        .then((response) => response.json())
                                        .then((response) => {
                                            resolve(response);
                                        })
                                        .catch((error) => {
                                            reject(error);
                                        })
                                    })
                                );
                            }
                        }

                        Promise.all(promisesArr)
                        .then((values) => {
                            for(let i = 0; i < values.length; i ++){
                                arr[i].venueInformation = values[i].venueInformation;
                                arr[i].scoreStatus = values[i].scoreStatus;
                                arr[i].teams = values[i].match;
                            }
                            resolve({matchs: arr, refreshing: false})
                        })
                    }
                });
            })
        })
        
    }

    componentWillMount(){
        this.getMatchs()
        .then(data => {
            console.log(data);
            this.setState(data)
        })
        .catch(err => {
            alert('Ha ocurrido un error en la conexión.')
        });
    }
    
    formatDate(date) {
        var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
        ];
      
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
      
        return monthNames[monthIndex] + ' ' + day + ' del ' + year;
    }

    getMatchInfo(matchId){
        console.log(matchId)
        return;
        return new Promise( (resolve, reject) => {
            fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/events/' + matchId + '.json')
            .then((response) => response.json())
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                alert('Ha ocurrido un error en la conexión.')
                reject(error);
            })
        });
    }

    renderMatch(item, index){

        let dateString  = item.date.toString();
        let year        = dateString.substring(0,4);
        let month       = dateString.substring(4,6);
        let day         = dateString.substring(6,8);
        let date        = new Date(year, month-1, day);

        
        
        localTime = new Date('1/1/2018 ' + item.scheduledStart + ' UTC-3');
        let hours = localTime.getHours();
        hours = (hours <= 10 ? '0' + hours : hours);

        let minutes = localTime.getMinutes();
        minutes = (minutes < 10 ? minutes + '0' : minutes);
        console.log(item)
        return(
            <View>
                {
                    item.level != 1?
                    <View style = {styles.time}>
                        <Text style = {styles.timeText}>
                            {item.levelName}
                        </Text>
                    </View>
                    : (
                        index % 6 == 0 ? 
                        <View style = {styles.time}>
                            <Text style = {styles.timeText}>
                                {'Jornada ' + (index/6 + 1) }
                            </Text>
                        </View>
                        : null
                    )
                }
                {}

                <TouchableOpacity
                    style = {{alignSelf: 'center'}}
                    onPress = {() => {
                        console.log(item)
                        if(date <= new Date())
                            this.props.navigation.push('match', {match : item});
                    }}
                    >
                    <View style = {[styles.match, {marginBottom: 5, marginTop: 5}]}>
                        <Text style = {styles.dateText}>{this.formatDate(date)}</Text>
                        <Text style = {styles.hourText}>{item.scheduledStart == null ? 'Hora no definida' : hours + ':' + minutes}</Text>
        
                        <View style = {{flexDirection: 'row', flex: 1}}>
                            <View style = {styles.team1}>
                                <View style = {styles.infoTeam}>
                                    <Image
                                        style={{width: 30, height: 30, alignSelf: 'center'}}
                                        source={this.state.icons[item.teams.homeTeamId]}
                                    />
                                    <Text style = {styles.nameTeam}>
                                        {item.teams.homeTeamName}
                                    </Text>
                                </View>
                            </View>
        
                            <View style = {styles.score}>
                                <Text style = {styles.scoreTeam}>
                                    {item.teams.homeTeamId === null || item.teams.awayTeamId === null || item.scoreStatus === {} || item.scoreStatus == null || item.scoreStatus[item.teams.homeTeamId].score == null ? '-' : item.scoreStatus[item.teams.homeTeamId].score }
                                </Text>
                                <Text style = {styles.separator}>
                                -
                                </Text>
                                <Text style = {styles.scoreTeam}>
                                    {item.teams.homeTeamId === null || item.teams.awayTeamId === null || item.scoreStatus === {} || item.scoreStatus == null || item.scoreStatus[item.teams.awayTeamId].score == null ? '-' : item.scoreStatus[item.teams.awayTeamId].score}
                                </Text>
                            </View>
                            
        
                            <View style = {styles.team2}>
                                <View style = {styles.infoTeam}>
                                    <Image
                                        style={{width: 30, height: 30, alignSelf: 'center'}}
                                        source={this.state.icons[item.teams.awayTeamId]}
                                    />
                                    <Text style = {styles.nameTeam}>
                                        {item.teams.awayTeamName}
                                    </Text>
                                </View>
                            </View>
        
                        </View>                
                    </View>
                </TouchableOpacity>
            </View>
            )
    }

    render(){
        return(
            <View style = {styles.container}>

                <Header navigation = {this.props.navigation}/>

                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        CALENDARIO
                    </Text>
                </View>

                <FlatList
                    onRefresh = {this.refresh}
                    refreshing = {this.state.refreshing}
                    data = {this.state.matchs}
                    renderItem = {({item, index}) => this.renderMatch(item, index)}
                    keyExtractor = {(item, index) => index.toString()}
                    style = {styles.scroll}
                    contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                </FlatList>

                <AdFooter/>

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    scroll: {
        flex: 1,
        width: width,
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    time: {
        marginTop: 10,
        marginBottom: 5,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    timeText:{
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    match: {
        paddingTop: 5,
        width: width * 0.95,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#0f1235'
    },

    dateText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 14
    },

    hourText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 12
    },

    team1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    team2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    infoTeam: {
        alignContent: 'center',
        justifyContent: 'center',
    },

    nameTeam: {
        textAlign: 'center',
        alignSelf: 'center',
        color: '#000',
        fontSize: 16,
    },
    
    separator: {
        marginLeft: 5,
        marginRight: 5,
        color: '#000',
        fontSize: 20
    },

    scoreTeam: {
        color: '#000',
        fontSize: 20
    },

    score: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    matchfooter: {
        flexDirection: 'row',
        backgroundColor: '#0f1235',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginTop: 5,
        paddingRight: 10,
        paddingLeft: 10,
        
    },

    stadium: {
        flex: 1,
        alignItems: 'flex-start'
    },

    status: {
        alignItems: 'flex-end'
    },

    statusText: {
        color: 'white',
        fontSize: 12
    },

    stadiumText: {
        color: 'white',
        fontSize: 12
    },

  });
  