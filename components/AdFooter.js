import React, {Component} from 'react';
import {Image, StyleSheet, Text, View, Dimensions, TouchableOpacity, Platform, SafeAreaView} from 'react-native';
import { 
    AdMobBanner, 
    AdMobInterstitial, 
    PublisherBanner,
    AdMobRewarded
  } from 'react-native-admob'

const {width, height} = Dimensions.get('window');

export default class AdFooter extends Component{
    constructor(props){
        super(props);
        this.state = {timePassed: false, events: {}}
    }

    render(){
        return(
            <SafeAreaView style = {styles.container}>
                
                <AdMobBanner
                adSize="smartBannerPortrait"
                adUnitID={Platform.OS === 'ios' ? "ca-app-pub-3710973902746391/7504779644" : "ca-app-pub-3710973902746391/8895303811"}
                testDevices={[AdMobBanner.simulatorId]}
                onAdFailedToLoad={error => console.log('Error al cargar el banner')}
                />
            </SafeAreaView>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width,
        flexDirection: 'row'
    },

    button: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    image: {
        width: 30, height: 30
    }
  });
  