import React, {Component} from 'react';
import {StatusBar, Alert, AsyncStorage, Image, Platform, StyleSheet, Text, View, Dimensions, FlatList, TouchableOpacity} from 'react-native';
import Header from './Header'
import AdFooter from './AdFooter'
import firebase from 'react-native-firebase';

const {width, height} = Dimensions.get('window');

export default class Welcome extends Component{
    constructor(props){
        super(props);
        this.state = {timePassed: false, matchs: [], refreshing: true,
            icons: {
                6546: require('../resources/logos/6546.png'),
                6539: require('../resources/logos/6539.png'),
                6538: require('../resources/logos/6538.png'),
                5751: require('../resources/logos/5751.png'),
                5155: require('../resources/logos/5155.png'),
                5154: require('../resources/logos/5154.png'),
                4417: require('../resources/logos/4417.png'),
                2248: require('../resources/logos/2248.png'),
                2247: require('../resources/logos/2247.png'),
                2067: require('../resources/logos/2067.png'),
                1773: require('../resources/logos/1773.png'),
                1292: require('../resources/logos/1292.png'),
                6549: require('../resources/logos/6549.png'),
                6540: require('../resources/logos/6540.png')
                }  
            
        
        }
    }
    
    async componentDidMount() {
        const channel = new firebase.notifications.Android.Channel(
            '2019',
            'futbolchapin',
            firebase.notifications.Android.Importance.Max
          ).setDescription('Canal de la notificacion');
        firebase.notifications().android.createChannel(channel);
        this.checkPermission();
        this.createNotificationListeners();
    }

    async checkPermission() {
        
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async removeItemValue(key) {
        try {
            await AsyncStorage.removeItem(key);
            return true;
        }
        catch(exception) {
            return false;
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmTokenNew');
        
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('fcmTokenNew', fcmToken);
                fetch('http://167.172.115.224:8000/registerToken', {
                    method: 'POST',
                    headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({token: fcmToken})
                })
                .then(response => response.json())
                .then(response => {
                    console.log(response);
                })
                .catch(error => {
                    this.removeItemValue('fcmTokenNew')
                    alert('Ha ocurrido un error con la conexión. ',error)
                })
            }
        }else{
            console.log("Token: " + fcmToken)
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }


    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const { title, body } = notification;
            //this.showAlert(title, body);
        });
      
        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body } = notificationOpen.notification;
            //this.showAlert(title, body);
        });
      
        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const { title, body } = notificationOpen.notification;
            //this.showAlert(title, body);
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage((message) => {
          //process data message
          console.log(JSON.stringify(message));
        });
    }

    showAlert(title, body) {
        Alert.alert(
        title, body,
        [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
        );
    }

    refresh = () => {
        this.setState({refreshing: true});
        this.getMatchs();
    }

    getMatchs(){
        fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/agendaMaM/es/agenda.json', {

        })
        .then((response) => response.json())
        .then((response) => {
            response = response.events;
            let arr = [];

            let promisesArr = [];

            let curr = new Date();
            let first =  curr.getDate() - curr.getDay() + (curr.getDay() === 0 ? -6 : 1);
            let firstday = new Date(curr.setDate(first));
            let lastday = new Date(curr.setDate(firstday.getDate()+6));

            for(let prop in response){
                response[prop].matchId = prop.split('.')[3];

                let dateString  = response[prop].date.toString();
                let year        = dateString.substring(0,4);
                let month       = dateString.substring(4,6);
                let day         = dateString.substring(6,8);
                let date        = new Date(year, month-1, day, 0, 0, 0);
                if(date >= firstday && date <= lastday){
                    arr.push(response[prop]);
                    promisesArr.push(this.getMatchInfo(response[prop].matchId));
                }
            }

            Promise.all(promisesArr)
            .then((values) => {
                for(let i = 0; i < values.length; i ++){
                    arr[i].venueInformation = values[i].venueInformation;
                    arr[i].scoreStatus = values[i].scoreStatus;
                }
                this.setState({matchs: arr, refreshing: false});
            })

            
        }).catch((error) => {
            this.setState({refreshing: false})
            alert("Ha ocurrido un error en la conexión.");
        });
    }

    componentWillMount(){
        this.getMatchs();
    }
    
    formatDate(date) {
        var monthNames = [
          "Enero", "Febrero", "Marzo",
          "Abril", "Mayo", "Junio", "Julio",
          "Agosto", "Septiembre", "Octubre",
          "Noviembre", "Diciembre"
        ];
      
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
      
        return monthNames[monthIndex] + ' ' + day + ' del ' + year;
    }

    getMatchInfo(matchId){
        return new Promise( (resolve, reject) => {
            fetch('http://futbolchapinenvivo.com/html/v3/htmlCenter/data/deportes/futbol/guatemala/events/' + matchId + '.json', {
                
            })
            .then((response) => response.json())
            .then((response) => {
                resolve(response);
            })
            .catch((error) => {
                reject(error);
            })
        });
    }

    renderMatch(item, index){

        let dateString  = item.date.toString();
        let year        = dateString.substring(0,4);
        let month       = dateString.substring(4,6);
        let day         = dateString.substring(6,8);
        let date        = new Date(year, month-1, day);

        
        
        localTime = new Date('1/1/2018 ' + item.scheduledStart + ' UTC-3');
        let hours = localTime.getHours();
        hours = (hours <= 10 ? '0' + hours : hours);

        let minutes = localTime.getMinutes();
        minutes = (minutes < 10 ? minutes + '0' : minutes);

        return(
            <TouchableOpacity
                onPress = {() => {
                    this.props.navigation.navigate('match', {match : item});
                }}
                >
                <View style = {[styles.match, {marginBottom: 5, marginTop: index == 0 ? 10 : 5}]}>
                    <Text style = {styles.dateText}>{this.formatDate(date)}</Text>
                    <Text style = {styles.hourText}>{hours + ':' + minutes}</Text>
    
                    <View style = {{flexDirection: 'row', flex: 1}}>
                        <View style = {styles.team1}>
                            <View style = {styles.infoTeam}>
                                <Image
                                    style={{width: 30, height: 30, alignSelf: 'center'}}
                                    source={this.state.icons[item.teams.homeTeamId]}
                                />
                                <Text style = {styles.nameTeam}>
                                    {item.teams.homeTeamName}
                                </Text>
                            </View>
                        </View>
    
                        <View style = {styles.score}>
                            <Text style = {styles.scoreTeam}>
                                {item.scoreStatus[item.teams.homeTeamId].score == null ? '-' : item.scoreStatus[item.teams.homeTeamId].score }
                            </Text>
                            <Text style = {styles.separator}>
                            -
                            </Text>
                            <Text style = {styles.scoreTeam}>
                                {item.scoreStatus[item.teams.awayTeamId].score == null ? '-' : item.scoreStatus[item.teams.awayTeamId].score}
                            </Text>
                        </View>
                        
    
                        <View style = {styles.team2}>
                            <View style = {styles.infoTeam}>
                                <Image
                                    style={{width: 30, height: 30, alignSelf: 'center'}}
                                    source={this.state.icons[item.teams.awayTeamId]}
                                />
                                <Text style = {styles.nameTeam}>
                                    {item.teams.awayTeamName}
                                </Text>
                            </View>
                        </View>
    
                    </View>
    
                    <View style = {styles.matchfooter}>
                        <View style = {styles.stadium}>
                            <Text style = {styles.stadiumText}>
                                {item.venueInformation.venue.stadium.stadiumName}
                            </Text>
                        </View>
                        <View style = {styles.status}>
                            <Text style = {styles.statusText}>
                                {item.statusId == 0 ? 'Sin iniciar' : (item.statusId == 1 ? 'En juego' : 'Finalizado')}
                            </Text>
                        </View>
                    </View>
                    
                
                </View>
            </TouchableOpacity>
            )
    }

    render(){
        return(
            <View style = {styles.container}>
                {Platform.OS === 'android' ? <StatusBar backgroundColor="#0f1235" barStyle="light-content" /> : null}
                <Header navigation = {this.props.navigation}/>

                <View style = {styles.title}>
                    <Text style = {styles.titleText}>
                        LIGA NACIONAL
                    </Text>
                </View>

                <FlatList
                    refreshing = {this.state.refreshing}
                    onRefresh = {this.refresh}
                    data = {this.state.matchs}
                    renderItem = {({item, index}) => this.renderMatch(item, index)}
                    keyExtractor = {(item, index) => index.toString()}
                    style = {styles.scroll}
                    contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>
                </FlatList>    

                <AdFooter/>                

            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'flex-start',
      paddingTop: Platform.OS === 'ios'  ? 20 : 0
    },

    scroll: {
        flex: 1,
        width: width,
    },

    text: {
        color: 'black'
    },

    header: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    title: {
        marginTop: 10,
        height: 35,
        backgroundColor: '#0f1235',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },

    titleText: {
        fontSize: 15,
        color: '#fff',
        fontWeight: 'bold'
    },

    match: {
        paddingTop: 5,
        width: width * 0.95,
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#0f1235'
    },

    dateText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 14
    },

    hourText: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 12
    },

    team1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    team2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },

    infoTeam: {
        alignContent: 'center',
        justifyContent: 'center',
    },

    nameTeam: {
        textAlign: 'center',
        alignSelf: 'center',
        color: '#000',
        fontSize: 16,
    },
    
    separator: {
        marginLeft: 5,
        marginRight: 5,
        color: '#000',
        fontSize: 20
    },

    scoreTeam: {
        color: '#000',
        fontSize: 20
    },

    score: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    matchfooter: {
        flexDirection: 'row',
        backgroundColor: '#0f1235',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        marginTop: 5,
        paddingRight: 10,
        paddingLeft: 10,
        
    },

    stadium: {
        flex: 1,
        alignItems: 'flex-start'
    },

    status: {
        alignItems: 'flex-end'
    },

    statusText: {
        color: 'white',
        fontSize: 12
    },

    stadiumText: {
        color: 'white',
        fontSize: 12
    },

  });
  