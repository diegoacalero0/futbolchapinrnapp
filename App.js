import {createStackNavigator, createAppContainer} from 'react-navigation'

import Welcome from './components/Welcome'
import Home from './components/Home'
import Match from './components/Match'
import Teams from './components/Teams'
import Players from './components/Players'
import Schedule from './components/Schedule'
import Positions from './components/Positions'
import ActiveNotifications from './components/ActiveNotifications'
import New from './components/New'
import Scorers from './components/Scorers'

const MainNavigator = createStackNavigator(
  {
    welcome: Welcome,
    home: Home,
    match: Match,
    teams: Teams,
    players: Players,
    positions: Positions,
    schedule: Schedule,
    activeNotifications: ActiveNotifications,
    new: New,
    scorers: Scorers
  },
  {
    headerMode: 'none',
    initialRouteName: 'welcome',
  }

);

const App = createAppContainer(MainNavigator);

export default App;
